<?php  
    include_once './includes/functions/data_connector/db-connector.php'; 
    include_once './includes/functions/function-register.php';
    include_once './includes/elements/header.php'; 
?>

    <main>
        <div class="container">
            <div class="row flex-row">
                <div class="statistic-tab">        
                    <div class="tournament-title">
                        <h3>Statistique champion</h3>
                    </div>
                    <div id="content" class="statistic-warrior">           
                        <div class="statistic-tournament-game">                            
                            <div class="top-tab">
                                <a href="/includes/subpage/element-statistic.php?user_id=1">
                                    <span class="description">CHAMPION</span>
                                    <span class="font-bg"></span>
                                    <img src="img/combattant1.png">
                                </a>
                            </div>
                        </div>            
                        <div class="statistic-tournament-game">                         
                            <div class="top-tab"> 
                                <a href="/includes/subpage/element-statistic.php?user_id=2">
                                    <span class="description">CHAMPION</span>
                                    <span class="font-bg"></span>
                                    <img src="img/combattant2.png">
                                </a>                           
                            </div>                            
                        </div>            
                        <div class="statistic-tournament-game"> 
                            <div class="top-tab">        
                                <a href="/includes/subpage/element-statistic.php?user_id=3">
                                    <span class="description">CHAMPION</span>
                                    <span class="font-bg"></span>
                                    <img src="img/combattant3.png">
                                </a>
                            </div>
                        </div>
                    </div>        
                </div>  
            </div>
        </div>
    </main>

<?php
    include_once './includes/elements/footer.php';
?> 