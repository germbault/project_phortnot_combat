/*
                FENÊTRE MODAL close
=========================================================*/

var modal = document.getElementById('activator');

document.body.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

/*
                 CAROUSSEL GALLERY 
=========================================================*/

var caroussel = {

    nbSlide: 0,
    nbCurrent: 1,
    elemCurrent: null,
    elem: null,
    timer: null,

    init: function (elem) {
        this.nbSlide = elem.find(".slide-gallery").length;

        // créer la pagination
        elem.append('<div class="navigation"></div>');
        for (var i = 1; i <= this.nbSlide; i++) {
            elem.find(".navigation").append("<span>" + i + "</span>");
        }
        elem.find(".navigation span").click(function () { caroussel.gotoSlide($(this).text()); })

        // Initialisation du caroussel
        this.elem = elem;
        elem.find(".slide-gallery").hide();
        elem.find(".slide-gallery:first").show();
        this.elemCurrent = elem.find(".slide-gallery:first");
        this.elem.find(".navigation span:first").addClass("active");

        // Création du timer
        this.timer = window.setInterval("caroussel.next()", 5000);
    },

    gotoSlide: function (num) {
        if (num == this.nbCurrent) { return false; }

        /* Animation en fadeIn/fadeOut */
        //this.elemCurrent.fadeOut();
        //this.elem.find("#slide" + num).fadeIn();
        /* Fin */

        /* Animation en slide */
        var sens = 1;
        if (num < this.nbCurrent) { sens = -1; }
        var cssStart = { "left": sens * this.elem.width() };
        var cssEnd = { "left": -sens * this.elem.width() };
        this.elem.find("#slide" + num).show().css(cssStart);
        this.elem.find("#slide" + num).animate({ "top": 0, "left": 0 }, 500);
        this.elemCurrent.animate(cssEnd, 500);
        /* Fin */

        this.elem.find(".navigation span").removeClass("active");
        this.elem.find(".navigation span:eq(" + (num - 1) + ")").addClass("active");
        this.nbCurrent = num;
        this.elemCurrent = this.elem.find("#slide" + num);
    },

    next: function () {
        var num = this.nbCurrent + 1;
        if (num > this.nbSlide) {
            num = 1;
        }
        this.gotoSlide(num);
    },

    prev: function () {
        var num = this.nbCurrent - 1;
        if (num < 1) {
            num = this.nbSlide;
        }
        this.gotoSlide(num);
    },
}

/*
                 MENU animé 
=========================================================*/


jQuery(document).ready(function ($) {
    caroussel.init($("#caroussel"));

    var positionTop = parseInt($(".top-tab a:first span.description").css("top"));

    // création de l'apparence opacle de l'image et ajout de description sur l'opacity
    $(".top-tab a").mouseover(function () {
        $(this).find("span.font-bg").show().stop().fadeTo(500, 0.7);
        $(this).find("span.description").css({
            opacity: 0,
            top: positionTop + 25
        }).animate({
            opacity: 1,
            top: positionTop
        });
    });

    // création d'effect de flash sur le menu
    $(".animate-bottom").click(function (e) {
        var parentOffset = $(this).offset(),
            cursorX = e.pageX - parentOffset.left,
            cursorY = e.pageY - parentOffset.top;

        $(this).children(".flash").remove();
        $(this).append('<div class="flash"></div>');
        $(this).children(".flash").css({
            "left": cursorX + "px",
            "top": cursorY + "px"
        })
    });

    // Apparition du logo et effect de rebond 
    // Apparition de la publicité
    // Apparition des images des champions 
    $(window).on('load', function () {

        var scale = 1;
        setTimeout(function () {
            scale = scale == 1 ? 1 : 1
            $('.wrap').css('transform', 'scale(' + scale + ')')
            $('.caroussel-slide').css('transform', 'scale(' + scale + ')')
            $('.top-tab').css('transform', 'scale(' + scale + ')')
        }, 100)

        doBounce($(".fa-biohazard"), 3, '10px', 500);
        function doBounce(element, times, distance, speed) {
            for (i = 0; i < times; i++) {
                element.animate({ marginTop: '-=' + distance }, speed)
                    .animate({ marginTop: '+=' + distance }, speed);
            }
        }
    });
});