<?php 
    include_once './includes/functions/data_connector/db-connector.php'; 
    include_once './includes/functions/function-register.php';
    include_once './includes/elements/header.php'; 
?>

    <main>
        <div class="container">
            <div class="row flex-row">
                <div class="left-width">
                    <div>
                        <div class="tournament-title">
                            <h3>Tournoi</h3>
                        </div>
                        <div class="tournament">
                            <div class="tournament-game">
                                <img src="img/combattant1.png">
                                <h3>PHORNEIN</h3>
                            </div>
                            <div class="tournament-game">
                                <h4 class="vs">VS</h4>                               
                            </div>
                            <div class="tournament-game">
                                <img src="img/combattant2.png">
                                <h3>MARSIEN</h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right-width">
                    <div class="tournament-title">
                        <h3>Résultat champion</h3>
                    </div>
                    <div class="champion-result-tab">
                        <div class="statistic-games">
                            <img src="img/combattant2.png">
                            <span>WIN</span>
                            <span>:</span>
                            <div class="game-time">
                                <p class="time">10:12</p>
                                <p class="date">11 Mai 2020</p>
                            </div>
                        </div>

                        <div class="statistic-games">
                            <img src="img/combattant1.png">
                            <span>WIN</span>
                            <span>:</span>
                            <div class="game-time">
                                <p class="time">18:42</p>
                                <p class="date">19 Mai 2020</p>
                            </div>
                        </div>

                        <div class="statistic-games">
                            <img src="img/combattant3.png">
                            <span>WIN</span>
                            <span>:</span>
                            <div class="game-time">
                                <p class="time">20:52</p>
                                <p class="date">28 Mai 2018</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php
    include_once './includes/elements/footer.php';
?> 