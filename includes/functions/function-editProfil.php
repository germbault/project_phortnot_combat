<?php
$error = null;
$msgSuccess = null;
$error1 = null;
$msgSuccess1 = null;

function validate_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validate_data($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// editer le profil de l'utilisateur
if (isset($_POST['formEdituser'])) {         
    $editFirst_name = validate_data($_POST['editFirst_name']);
    $editLast_name = validate_data($_POST['editLast_name']);
    $editEmail = validate_data(validate_email($_POST['editEmail']));
    $editPhone = validate_data($_POST['editPhone']);
    $editDate_birth = validate_data($_POST['editDate_birth']);

    try {
        if(isset($editFirst_name) and !empty($editFirst_name) and $editFirst_name != $user["first_name"]) {
            if (preg_match("/^[a-zA-Z0-9 -]+$/",$editFirst_name)) {
                $insertFirst_name =  $conn->prepare("UPDATE user SET first_name = ? WHERE id = ?"); 
                $insertFirst_name->execute(array($editFirst_name, $_SESSION['auth']));
                $msgSuccess = "Votre nom a bien été mis a jour !";
            } else {    
                $error = "Nom invalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
            }  
        }
        
        if(isset($editLast_name) and !empty($editLast_name) and $editLast_name != $user["last_name"]) {
            if (preg_match("/^[a-zA-Z0-9 -]+$/",$editLast_name)) {
                $insertLast_name =  $conn->prepare("UPDATE user SET last_name = ? WHERE id = ?"); 
                $insertLast_name->execute(array($editLast_name, $_SESSION['auth']));
                $msgSuccess = "Votre prénom a bien été mis a jour !";
            } else {    
                $error = "Prénom invalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
            } 
        }
        
        if(isset($editEmail) and !empty($editEmail) and $editEmail != $user["email"]) {
            if ($editEmail) {
                $insertMail =  $conn->prepare("UPDATE user SET email = ? WHERE id = ?"); 
                $insertMail->execute(array($editEmail, $_SESSION['auth']));
                $msgSuccess = "Votre courriel a bien été mis a jour !";
            } else {    
                $error = " Votre adresse mail n'est pas valide !";        
            } 
        }

        if(isset($editPhone) and !empty($editPhone) and $editPhone != $user["phone"]) {
            if (preg_match("/^[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4}$/",$editPhone)) {
                $insertPhone =  $conn->prepare("UPDATE user SET phone = ? WHERE id = ?"); 
                $insertPhone->execute(array($editPhone, $_SESSION['auth']));
                $msgSuccess = "Votre No de téléphone bien été mis a jour !";
            } else {    
                $error = " Votre No de téléphone n'est pas valide !";        
            } 
        }

        if(isset($editDate_birth) and !empty($editDate_birth) and $editDate_birth != $user["date_birth"]) {            
                $insertBirth =  $conn->prepare("UPDATE user SET date_birth = ? WHERE id = ?"); 
                $insertBirth->execute(array($editDate_birth, $_SESSION['auth']));
                $msgSuccess = "Votre date de naissance a bien été mis a jour !";           
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }

}

// Éditer le mot de passe de l'utilisateur
if (isset($_POST['formEditpassword'])) {        
    $editPass_word = $_POST["editPass_word"];
    $editPassword_confirm = $_POST["editPassword_confirm"];

    try {
        if(isset($editPass_word) AND isset($editPassword_confirm)) {
            if(!empty($editPass_word) AND !empty($editPassword_confirm)) {
                if ($editPass_word == $editPassword_confirm) {                                            
                    $hashed_password = password_hash($editPass_word, PASSWORD_BCRYPT);
                   
                    $insertMdp =  $conn->prepare("UPDATE user SET pass_word = ? WHERE id = ?"); 
                    $insertMdp->execute(array($hashed_password, $_SESSION['auth']));
                    $msgSuccess1 = "Votre mot de passe a bien été mis a jour !";

                } else {
                    $error1 = " Vos mots de passe ne correspondent pas !";
                }
            }
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}