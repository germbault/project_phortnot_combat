<?php
$error = null;
$msgSuccess = null;

function form_values($first_name) {
    echo (isset($_POST[$first_name]) ? $_POST[$first_name] : "");
}

function validate_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validate_data($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['formComments'])) {         
    $first_name = validate_data($_POST['first_name']);
    $last_name = validate_data($_POST['last_name']);
    $email = validate_data(validate_email($_POST['email']));
    $comment = validate_data($_POST["comment"]);
    try {
        if(isset($first_name) AND isset($last_name) AND isset($email) AND isset($comment)) {
            if(!empty($first_name) AND !empty($last_name) AND !empty($email) AND !empty($comment)) {
                if (preg_match("/^[a-zA-Z0-9 -]+$/",$first_name) AND preg_match("/^[a-zA-Z0-9 -]+$/",$last_name)) {        
                    if ($email) {
                        $conn = connect();
                                $pdo = $conn->prepare("INSERT INTO contact (first_name, last_name, email, comments) 
                                            VALUES(:first_name, :last_name, :email, :comments )");

                                $pdo->execute(array(
                                    ':first_name' => $_POST["first_name"],
                                    ':last_name' =>  $_POST["last_name"],                                
                                    ':email' =>      $_POST["email"],
                                    ':comments' =>   $_POST["comment"]
                                                            
                                ));
                                $msgSuccess = "Votre compte a bien été créé !";
                                                        
                    } else {
                        $error = " Votre message à bien été envoyé, !";
                    }
                } else {    
                    $error = "Nom ou prénom ivalide ( majuscule, minuscule seulement et caractère spéciaux non accepté )";        
                }               
            } else {    
                $error = "Tous les champs doivent être complétés !";        
            }
        }
    } catch (PDOException $e) {
        $error = $e->getMessage();        
    }
}