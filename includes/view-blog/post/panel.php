<?php

    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once  folder('/includes/functions/data_connector/db-connector.php'); 
    include_once  folder('/includes/functions/function-register.php');
    include_once  folder('/includes/elements/header.php');
    

        // Requête pour récuprer les 12 articles de la bd
        $conn = connect();
        $query = $conn->query('SELECT * FROM post ORDER BY created_at DESC');
        $posts = $query->fetchAll();
    
    ?>
    
        <main>
            <section class="container"> 
                    <h1>Mon panel (liste d'article)</h1>           
                    <table class="content-blog">
                        <thead class="head-bg">
                            <th>#</th>
                            <th>Titre</th>
                            <th>Actions</th>
                        </thead>
                        <tbody class="tbody-border">
                            <?php foreach($posts as $post): ?>
                            <tr class="flex-row">
                                <td>#<?= $post['id'] ?></td>
                                <td><a href="/includes/view-blog/post/article.php?id=<?= $post['id'] ?>" class="hover-link"><?= $post['title'] ?></a></td> <!--titre de l'article-->
                                <td class="end">
                                    <a href="/includes/view-blog/post/news.php" class="create-bottom">Créer </a>
                                    <a href="/includes/view-blog/post/edit.php?edit=<?= $post['id'] ?>" class="edit-bottom">Éditer</a>
                                    <a href="/includes/view-blog/post/delete.php?id=<?= $post['id'] ?>" class="delete-bottom" onclick="return confirm('Voulex vous vraiment effectuer cette action ?')">                                   
                                        Supprimer
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach ?>                      
                        </tbody>
                    </table>      
            </section>
        </main>

<?php
    include_once  folder('/includes/elements/footer.php');
?>