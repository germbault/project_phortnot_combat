<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once  folder('/includes/functions/data_connector/db-connector.php'); 
    include_once  folder('/includes/functions/function-register.php');
    include_once  folder('/includes/elements/header.php');
    
    include_once  folder('/vendor/autoload.php');
    use App\Helpers\Text;
    use App\Model\Post;

    // Requête pour récuprer les 12 articles de la bd
    $conn = connect();
    $query = $conn->query('SELECT * FROM post ORDER BY created_at DESC LIMIT 12');
    $posts = $query->fetchAll(PDO::FETCH_CLASS, Post::class);

?>

    <main>
        <section class="container">                 
            <h1>Mon blog</h1>
            <div class="card-flex">
            <?php foreach($posts as $post): ?>            
                <div class="card-row">
                    <div class="card">
                        <h5 class="card-title"><?= htmlentities($post->getTitle()) ?></h5>
                        <p class="card-date"><?= $post->getCreatedAt()->format('d F Y') ?></p>
                        <p><?= $post->getExcerpt() ?></p>
                    </div>
                </div>           
            <?php endforeach ?>
            </div>        
        </section>
    </main>

<?php
    include_once  folder('/includes/elements/footer.php');
?> 
