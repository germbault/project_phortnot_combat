<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once folder('/includes/functions/data_connector/db-connector.php'); 
    include_once folder('/includes/functions/function-blog.php');
    include_once folder('/includes/elements/header.php');       
?>
    <main>
        <h2 class="dash-h2 container">Modification de l'article</h2>        
        <section class="content container">
            <h2><span class="ico_user"></span>Modifier l'article</h2>
            <?php
                if (isset($error)) { echo "<div class='error-php'>" . $error . "</div>"; }
                
                if (isset($msgSuccess)) { echo "<div class='succes-php'>" . $msgSuccess . "</div>"; }
            ?>
            <form id="#" action="#" method="POST">
                <table>
                    <tbody>
                        <tr>
                            <td>Titre<i class="fas fa-user-alt"></i></td>
                            <td><input class="input-table" name="edit_title" type="text" value=""></td>
                        </tr>

                        <tr>
                            <td>Slug<i class="fas fa-user-alt"></i></span></td>
                            <td><input class="input-table" name="edit_slug" type="text" value=""></td>
                        </tr> 
                        
                        <tr>
                            <td>contenu<i class="fas fa-envelope"></i></td>
                            <td><textarea class="input-table" name="edit_content" id="comment" placeholder="Entrez votre contenu ici!" 
                                  cols="30" rows="10"></textarea>
                        </tr>
                                                                                  
                        <tr>
                            <td class="td-buttom" colspan="2">
                            <button class="ajust-buttom" name="formEditBlog" type="submit">Enregistrer</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </section>    
    </main>

<?php
    include_once folder('/includes/elements/footer.php');
?> 