<?php

    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once  folder('/includes/functions/data_connector/db-connector.php'); 
    include_once  folder('/includes/functions/function-register.php');
    include_once  folder('/includes/elements/header.php');
    
    // Permet d'afficher la totalité de l'article à partir du lien dans une nouvelle page
    if (isset($_GET['id']) AND !empty($_GET['id'])) {
        $get_id = htmlspecialchars($_GET['id']);

        $conn = connect();
        $article = $conn->prepare("SELECT * FROM post WHERE id = ?");
        $article->execute(array($get_id));

        if ($article->rowcount() == 1) {
            $article = $article->fetch();
            $title = $article['title'];
            $slug = $article['slug'];
            $content = $article['content'];
        }else {
            die("Cet article n'existe pas !");
        }
    }else {
        die('Erreur');
    }

?>

    <main>
        <section class="container">            
            <h1><?= $title ?></h1>
            <p><?= $slug ?></p>
            <p><?= $content ?></p>     
        </section>
    </main>

<?php
    include_once  folder('/includes/elements/footer.php');
?>