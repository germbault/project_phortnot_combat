    <footer id="footer">
        <div class="container">
            <div class="row flex-row-footer">
                <div class="grid">
                    <h2 class="color_green">Commentaires</h2>
                    <ul class="list_2">
                        <li><a href="#">Dernier commentaire</a></li>
                        <li><a href="#">PlayStation</a></li>
                        <li><a href="#">Xbox</a></li>
                        <li><a href="#">PC</a></li>
                    </ul>
                </div>
                <div class="grid">
                    <h2 class="color_green">Nouvelles</h2>
                    <ul class="list_2">
                        <li><a href="#">Dernière nouvelle</a></li>
                        <li><a href="#">PlayStation</a></li>
                        <li><a href="#">Xbox</a></li>
                        <li><a href="#">PC</a></li>
                    </ul>
                </div>
                <div class="grid">
                    <h2 class="color_green">Contactez-nous</h2>
                    <ul class="list_2">
                        <li><a href="#">texte</a></li>
                        <li><a href="#">texte</a></li>
                        <li><a href="#">texte</a></li>
                        <li><a href="#">texte</a></li>
                        <li><a href="#">texte</a></li>
                    </ul>
                </div>
                <div class="grid">
                    <h2 class="color_green">Suivez nous</h2>
                    <ul class="social-list">
                        <li><a class="fab fa-facebook-f" href="#"></a></li>
                        <li><a class="fab fa-rss" href="#"></a></li>
                        <li><a class="fab fa-twitter" href="#"></a></li>
                        <li><a class="fab fa-google-plus-g" href="#"></a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div>
                    <div class="info">
                        <h2><a href="/index.php"><span class="fa fa-biohazard"></span> Phortnot <br> Combat</a></h2>
                        <p>© <span id="copyright-year">2020</span> | <a href="#">Tous droits réservés</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  
    <script src="/js/vendor/modernizr-3.7.1.min.js"></script>
   
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-3.5.1.min.js"><\/script>')</script>
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>

    <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
    <script>
        window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
    </script>
    <script src="https://www.google-analytics.com/analytics.js" async></script>
</body>
</html>