<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <title>Phortnot Combat</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->

    <link rel="stylesheet" href="/css/normalize.css">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css"></link>

    <meta name="theme-color" content="#fafafa">
</head>

<body>
    <header id="header">
        <div class="container">
            <div class="wrap_1">
                <div class="logo">
                    <div class="wrap">
                        <h1>
                            <a href="#"><span class="fa fa-biohazard"></span> Phortnot <br> Combat</a>
                        </h1>
                    </div>
                </div>

                <div class="user-link-flex">
                    <ul class="user-link">
                        <?php if (isset($_SESSION['auth'])): ?>
                        <li><a href="/logout.php"><span><i
                            class="fa fa-user"></i>SE DÉCONNECTER</span></a></li>
                            <li class="edit-profil"><a href="/includes/subpage/dashboard.php"><span><i class="fas fa-th-list"></i>EDITER MON PROFIL</span></a></li>
                            <?php if ($_SESSION['admin'] == 1) : ?>                         
                                <li class="edit-profil"><a href="/includes/view-blog/post/panel.php"><span><i class="fas fa-user-cog"></i>Administrateur</span></a></li>
                            <?php endif; ?>
                        <?php else: ?>
                        <li><a href="/register.php"><span><i
                            class="fa fa-user"></i>S'IDENTIFIER | S'INSCRIRE</span></a></li>
                        <?php endif; ?>
                    </ul>

                    <form id="search" action="search/search.php" method="GET" accept-charset="utf-8">
                        <label class="input_wrap" for="search">
                            <input type="search" name="s" placeholder="Recherche:">
                        </label>
                        <a class="fa fa-search" onclick="document.getElementById('search').submit()"></a>
                    </form>
                </div>
            </div>
        </div>

        <div class="container">
            <nav>
                <ul class="menu">
                    <li class="current"><a class="fa-home" href="/index.php"></a></li>
                    <li class="animate-bottom"><a href="/champion.php#">Champion</a></li>
                    <li class="animate-bottom"><a href="/statistique.php#">Statistique</a></li>

                    <li class="sub-menu" class="animate-bottom">
                        <a href="#">Boutique</a>
                        <ul>           
                            <li class="sub-menu-1"><a href="#">Équipement</a>
                                <ul>
                                    <li><a href="/includes/subpage/weapons.php">Armes</a></li>
                                    <li><a href="/includes/subpage/armors.php">Armures</a></li>
                                    <li><a href="#">Options</a></li>
                                </ul>
                            </li>
                            <li><a href="/includes/subpage/gallery.php">Gallerie d'image</a></li>
                        </ul>
                    </li>

                    <li class="animate-bottom"><a href="/includes/view-blog/post/blog.php#">Blog</a></li>
                    <li class="animate-bottom"><a href="/contact.php#">Contacts</a></li>
                </ul>
                <select class="menu select-menu" onChange="location = this.options[this.selectedIndex].value;">
                    <option value="#">Navigation à....</option>
                    <option value="index.php">&nbsp;Accueil</option>
                    <option value="champion.php">&nbsp;Champion</option>
                    <option value="statistique.php">&nbsp;Statistique</option>
                    <option value="#">&nbsp;Boutique</option>
                    <option value="#">&nbsp;Équipement</option>
                    <option value="/includes/subpage/weapons.php">––&nbsp;Armes</option>
                    <option value="/includes/subpage/armors.php">––&nbsp;Armures</option>
                    <option value="#">––&nbsp;Option</option> 
                    <option value="/includes/subpage/gallery.php">*&nbsp;Galerie</option>                                       
                    <option value="blog.php">&nbsp;Blog</option>
                    <option value="contact.php">&nbsp;Contacts</option>
                </select>
            </nav>
        </div>
    </header>    