<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once  folder('/includes/functions/data_connector/db-connector.php'); 
    include_once  folder('/includes/functions/function-register.php');
    include_once  folder('/includes/elements/header.php');
?>

    <main>
        <section class="container">
            <div class="row flex-row">
                <div class="statistic-tab">        
                    <div class="tournament-title">
                        <h3>Galerie d'image</h3>
                    </div>
                    <div class="gallery shop">
                        <div id="caroussel">                               
                            <div id="slide1" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/01.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div> 

                            <div id="slide2" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/02.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide3" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/03.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide4" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/04.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide5" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/05.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide6" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/06.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide7" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/07.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide8" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/08.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide9" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/09.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>

                            <div id="slide10" class="slide-gallery">                                
                                <div class="visual-gallery">                                
                                    <img src= "/img/shop-image/gallerie/10.png">                                    
                                </div>
                                <div class="info-text">                                                                
                                    <p>Source : https://wall.alphacoders.com/</p>                                                                                                                                  
                                </div>                                                          
                            </div>
                        </div>                                   
                    </div>                          
                </div>  
            </div>
        </section>
    </main>

<?php
    include_once  folder('/includes/elements/footer.php');
?> 