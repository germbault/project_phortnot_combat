<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once folder('/includes/functions/function-debug.php');
    include_once folder('/includes/functions/data_connector/db-connector.php'); 
    include_once folder('/includes/elements/header.php');
    
    if (isset($_SESSION['auth'])) {

        // Pour afficher les informations dans les champs
        $conn = connect();
        $requser = $conn->prepare("SELECT * FROM user WHERE id = ?");
        $requser->execute(array($_SESSION['auth']));
        $user = $requser->fetch();  

        include_once folder('/includes/functions/function-editProfil.php');
?>
    <main>
        <h2 class="dash-h2 container">Tableau de bord</h2>
        <p class="dash-p container">Bonjour <?php echo $_SESSION["pseudo"]; ?></p>        
        <section class="content container">
            <h2><span class="ico_user"></span> Mon profil</h2>
            <?php
                if (isset($error)) { echo "<div class='error-php'>" . $error . "</div>"; }
                
                if (isset($msgSuccess)) { echo "<div class='succes-php'>" . $msgSuccess . "</div>"; }
            ?>
            <form id="#" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                <table>
                    <tbody>
                        <tr>
                            <td>Nom<i class="fas fa-user-alt"></i></td>
                            <td><input class="input-table" name="editFirst_name" type="text" value="<?= $user["first_name"]; ?>"></td>
                        </tr>

                        <tr>
                            <td>Prenom<i class="fas fa-user-alt"></i></span></td>
                            <td><input class="input-table" name="editLast_name" type="text" value="<?= $user["last_name"]; ?>"></td>
                        </tr>
                        
                        <tr>
                            <td>Adresse email<i class="fas fa-envelope"></i></td>
                            <td><input class="input-table" name="editEmail" type="email" value="<?= $user["email"]; ?>"></td>
                        </tr>

                        <tr>
                            <td>Numéro de téléphone<i class="fas fa-phone-alt"></i></td>
                            <td><input class="input-table" name="editPhone" type="text" value="<?= $user["phone"]; ?>"></td>
                        </tr>

                        <tr>
                            <td>Date de naissance<i class="fas fa-table"></i></td>
                            <td><input class="input-table" name="editDate_birth" type="text" value="<?= $user["date_birth"]; ?>"></td>
                        </tr>
                                                                        
                        <tr>
                            <td class="td-buttom" colspan="2">
                            <button class="ajust-buttom" name="formEdituser" type="submit">Enregistrer</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </section>

        <section class="content container">
            <h2><span class="ico_user"></span> Éditer votre mot de passe</h2>
            <?php
                if (isset($error1)) { echo "<div class='error-php'>" . $error1 . "</div>"; }
                
                if (isset($msgSuccess1)) { echo "<div class='succes-php'>" . $msgSuccess1 . "</div>"; }
            ?>
            <form id="update-account" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                <table>
                    <tbody>
                        <tr>
                            <td>Nouveau mot de passe<i class="fas fa-lock"></i></td>
                            <td><input class="input-table" name="editPass_word" type="password" value=""></td>
                        </tr>
                        
                        <tr>
                            <td>confirmation du MPD<i class="fas fa-lock"></i></td>
                            <td>
                            <input class="input-table" name="editPassword_confirm" type="password" value=""></td>
                        </tr>
                                                                        
                        <tr>
                            <td class="td-buttom" colspan="2">
                                <button class="ajust-buttom" name="formEditpassword" type="submit">Enregistrer</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </section>         
    </main>
<?php } else { ?>        
        <script>window.location = 'http://phortnot.project.io/register.php'; </script>
<?php       
    }
    include_once folder('/includes/elements/footer.php');
?> 