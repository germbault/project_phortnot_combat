<?php
    include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
    include_once  folder('/includes/functions/data_connector/db-connector.php'); 
    include_once  folder('/includes/functions/function-register.php');
    include_once  folder('/includes/elements/header.php');
    
    $arr_img = [
        ["img" => "/img/shop-image/armors/armure01.png"],
        ["img" => "/img/shop-image/armors/armure02.png"],
        ["img" => "/img/shop-image/armors/armure03.png"],
        ["img" => "/img/shop-image/armors/armure04.png"],
        ["img" => "/img/shop-image/armors/armure05.png"],
        ["img" => "/img/shop-image/armors/armure06.png"],
        ["img" => "/img/shop-image/armors/armure07.png"],
        ["img" => "/img/shop-image/armors/armure08.png"],
        ["img" => "/img/shop-image/armors/armure09.png"],
        ["img" => "/img/shop-image/armors/armure10.png"]                               
    ];
?>

    <main>
        <section class="container">
            <div class="row flex-row">
                <div class="statistic-tab">        
                    <div class="tournament-title">
                        <h3>Boutique des armures</h3>
                    </div>
                    <div class="statistic-warrior shop">
                        <div id="carrousel-gallerie">
                            <?php                                                           
                                foreach ($arr_img as $loadImage) {                                                                
                            ?>    
                            <div id="slide1" class="slide">                                
                                <div class="visual-image">
                                    <img src= <?php echo $loadImage["img"]; ?>>
                                    <a href="#" class="visual-buttom">Acheter</a>
                                </div>
                                <div class="visual-info">
                                    <div class="witdh38">
                                        <p>Endurance +10</p>
                                        <p>Force +7</p>
                                        <p>Vitalité +17</p>
                                    </div>
                                    <div class="witdh62">
                                        <p><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                        <i class="fas fa-star-half-alt"></i><i class="far fa-star"></i></p>
                                        <div class="svg-wrapper" onclick="document.getElementById('activator').style.display='block'" style="width:auto;">                                            
                                            <svg version="1.1" baseProfile="tiny" id="Calque_1"
                                                xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 110.4 27.4"
                                                xml:space="preserve">
                                                <g>                                                    
                                                    <rect class="shape" rx="8" ry="8" stroke-linejoin="round" stroke-miterlimit="10" width="109.4" height="26.4"/>                                               
                                                    <text transform="matrix(1 0 0 1 7.3935 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">A</text>
                                                    <text transform="matrix(1 0 0 1 15.2197 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">V</text>
                                                    <text transform="matrix(1 0 0 1 23.0312 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">E</text>
                                                    <text transform="matrix(1 0 0 1 29.9199 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">R</text>
                                                    <text transform="matrix(1 0 0 1 37.5771 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">T</text>
                                                    <text transform="matrix(1 0 0 1 44.5351 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">I</text>
                                                    <text transform="matrix(1 0 0 1 47.8808 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">S</text>
                                                    <text transform="matrix(1 0 0 1 54.7832 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">S</text>
                                                    <text transform="matrix(1 0 0 1 61.6845 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">E</text>
                                                    <text transform="matrix(1 0 0 1 68.5732 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">M</text>
                                                    <text transform="matrix(1 0 0 1 79.8291 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">E</text>
                                                    <text transform="matrix(1 0 0 1 86.7168 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">N</text>
                                                    <text transform="matrix(1 0 0 1 95.9287 18.4758)" fill="#FFFFFF" font-family="'MyriadPro-Regular'" font-size="14px">T</text>
                                                </g>
                                            </svg>                                            
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <?php
                                }
                            ?>                           
                        </div>                                   
                    </div>                          
                </div>  
            </div>
        </section>

        <section id="activator" class="modal">
            <form class="modal-content animate">
                <div class="close">
                    <span onclick="document.getElementById('activator').style.display='none'" title="Close Modal">&times;</span>
                </div>
                <div>
                    <div class="container-form">
                        <p class="font-size1"><strong>!! IMPORTANT !!</STRONG></P>
                        <p class="font-size2">Tout achat est lié a la signature d'un contrat de non responsabilité qui dégage la boutique de toute responsabilité 
                            lié a l'utilisation des armes,armures ou des options lié également au items.</p>
                    </div>
                </div>
                <div class="container-form">
                    <button type="button" onclick="document.getElementById('activator').style.display='none'"
                    class="cancelbtn">Revenir</button>
                </div>
            </form>
        </section>
    </main>

<?php
    include_once  folder('/includes/elements/footer.php');
?> 