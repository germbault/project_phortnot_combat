<?php
include_once  $_SERVER['DOCUMENT_ROOT'].'/includes/functions/tools/folder-function.php';
include_once folder('/includes/functions/function-debug.php');
include_once folder('/includes/functions/data_connector/db-connector.php');
include_once folder('/includes/functions/function-statistic.php'); 
include_once folder('/includes/functions/function-register.php'); 
include_once folder('/includes/elements/header.php'); 
  
    $id  = $_GET['user_id'];

    if($id == 1) {
        $image = "/img/combattant1.png";        
    }

    if($id == 2) {
        $image = "/img/combattant2.png";        
    }

    if($id == 3) {
        $image = "/img/combattant3.png";        
    }
?>

    <main>
        <?php
        if(isset($_GET['user_id'])){
            $data = stat_champion($_GET['user_id']);
            foreach ($data as $row){  
        ?>                
        <div class="container">
            <div class="row1">                 
                <div class="tournament-title">
                    <h3>Statistique champion #1</h3>
                </div>
                <div class="statistic-warrior">           
                    <div class="statistic-tournament-game">
                        <img src= <?php echo $image ?>>                    
                    </div>
                    <div class="statistic-tournament-game left-text">
                        <h3>MES CUML DIA SED INENIAS INGERTO.</h3>
                        <p>Publié le 02 Juin 2020</p>
                        <p><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star-half-alt"></i><i class="far fa-star"></i></p>
                        <p>Dolor nunc vule putateulr ips dol consec.Donec semp ertet laciniate ultricie upien disse comete dolo lectus fgilla 
                            itollicil tua ludin dolor nec met.</p>
                    </div>
                    <div class="statistic-tournament-game">
                        <div class="tournament-title initial-buttom">
                            <h3>Résultat / Victoire</h3>
                        </div>                       
                        <div class="stat-flex">
                            <div class="flex1">
                                <p class="color1">Vitoire</p>
                                <p class="color3"><?php echo $row["victory"]; ?>victoire total au classement</p>
                            </div>
                            <div class="flex2">
                                <p class="color2">Palier</p>
                                <p class="color4">Classé <?php echo $row["ranking"]; ?> ième régional.</p>
                            </div>
                            <div class="flex1">
                                <p class="color1">Date</p>
                                <p class="color3">Dernière victoire le <?php echo $row["date"]; ?></p>
                            </div>                           
                        </div>                        
                    </div>        
                </div>
            </div>    
        </div>
    </main>
    <?php
            }
        }
    ?>

<?php
    include_once folder('/includes/elements/footer.php');
?> 