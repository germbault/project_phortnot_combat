<?php
    include_once './includes/functions/data_connector/db-connector.php'; 
    include_once './includes/functions/function-register.php'; 
    include_once './includes/elements/header.php';   
?>

    <main>
        <div class="container caroussel">                 
            <div class="baniere caroussel-view">
                <div id="caroussel-slide1" class="acte-1 caroussel-slide" >
                    <a href="#caroussel-slide3" class="caroussel-prev"></a>
                    <a href="#caroussel-slide2" class="caroussel-next"></a>
                    <img src="/img/acte-1-background.png" class="acte-1-background">
                    <img src="/img/vortex.gif" class="vortex">
                    <img src="/img/épé1.png" class="epe">
                    <img src="/img/hache1.png" class="hache">
                    <h2>phortnot combat</h2>
                    <h3>combattre est votre </p><br>seule issue</h3>
                </div>

                <div id="caroussel-slide2" class="acte-2 caroussel-slide" >
                    <a href="#caroussel-slide1" class="caroussel-prev"></a>
                    <a href="#caroussel-slide3" class="caroussel-next fas"></a>
                    <img src="/img/acte-2-background.png" class="acte-2-background">
                    <img src="/img/personnage-1.png" class="personnage-1">
                    <img src="/img/personnage-2.png" class="personnage-2">
                    <img src="/img/épé1.png" class="epe-1">
                    <img src="/img/hache1.png" class="hache-1">
                    <img src="/img/flame.png" class="flame-1">
                    <img src="/img/flame.png" class="flame-2">
                </div>

                <div id="caroussel-slide3" class="acte-3 caroussel-slide" >
                    <a href="#caroussel-slide2" class="caroussel-prev"></a>
                    <a href="#caroussel-slide1" class="caroussel-next"></a>
                    <img src="/img/acte-3-background.png" class="acte-3-background">
                    <img src="/img/vortex.gif" class="vortex-1">
                    <img src="/img/personnage-2.png" class="personnage-3">
                    <img src="/img/flame.png" class="flame-3">
                    <img src="/img/flame.png" class="flame-4">
                    <h2>phortnot combat<br><span>WIN</span></h2>
                </div>
            </div>
            <div>
                <p>Pour essayer d'endiguer le problème de surpopulation des Phrakéens, un tournoi de combat mortel fut mis en place.Chaques tournoi comporte 100 
                    opposants qui s'affronteront 1 contre 1 dans une arène. Le gagnant obtient le droit à la détéléportation dans le vide spatio-temporel 
                    et la dématérialisation quantique par induction de quarks aspirer par un vortex, seul moyen de réussir à mourir</p>
            </div>      
        </div>
    </main>

<?php
    include_once './includes/elements/footer.php';
?> 



 