<?php
    include_once './includes/functions/data_connector/db-connector.php'; 
    include_once './includes/functions/function-register.php';
    include_once './includes/elements/header.php';   
?>
    <main>
        <div class="user-flex container">              
            <section class="user-content">
                <h2><span class="ico_user"></span>Se connecter</h2>
                <?php
                    if (isset($error1)) { echo "<div class='error-php'>" . $error1 . "</div>"; }
                    
                    if (isset($msgSuccess1)) { echo "<div class='succes-php'>" . $msgSuccess1 . "</div>"; }
                ?>          
                <form id="login" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                    <input type="hidden" name="login">
                    <table>
                        <tbody>
                            <tr>                              
                                <td>Nom d'utilisateur<i class="fas fa-user-alt"></i></td>
                                <td><input id=mail_login name=mail_login type=email placeholder="Saisissez votre adresse e-mail"
                                class="input-form"></td>
                            </tr>

                            <tr>
                                <td>Mot de passe<i class="fas fa-unlock"></i></span></td>
                                <td><input id=pass_word_login name=pass_word_login type=password placeholder="Mot de passe"
                                class="input-form"></td>
                            </tr>                                  
                                                                            
                            <tr>
                                <td class="td-buttom" colspan="2">
                                <button class="user-ajust-buttom" name="formLogin" type="submit">Se connecter</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="psw">
                        <span>Vous avez oublié votre <a href="#">mot de passe?</a></span>
                    </div>
                </form>
            </section>

            <section class="user-content">
                <h2><span class="ico_user"></span>S'inscrire</h2>  
                <?php
                    if (isset($error)) { echo "<div class='error-php'>" . $error . "</div>"; }
                    
                    if (isset($msgSuccess)) { echo "<div class='succes-php'>" . $msgSuccess . "</div>"; }
                ?>
                <form id="register" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                    <input type="hidden" name="register">
                    <table>
                        <tbody>
                            <tr>                               
                                <td>Nom<i class="fas fa-user-alt"></i></td>
                                <td><input id=first_name name=first_name type=text placeholder="Nom" class="input-form" value="<?php form_values("first_name") ?>"></td>
                            </tr>

                            <tr>                                
                                <td>Prénom<i class="fas fa-user-alt"></i></td>
                                <td><input id=last_name name=last_name type=text placeholder="Prenom" class="input-form" value="<?php form_values("last_name") ?>"></td>
                            </tr>

                            <tr>                               
                                <td>Courriel<i class="fas fa-envelope"></i></td>
                                <td><input id=email name=email type=email placeholder="courriel" class="input-form" value="<?php form_values("email") ?>"></td>
                            </tr>

                            <tr>                               
                                <td>Téléphone<i class="fas fa-phone-alt"></i></td>
                                <td><input id=phone name=phone type=text placeholder="# téléphone" class="input-form" value="<?php form_values("phone") ?>"></td>
                            </tr>

                            <tr>                               
                                <td>Date de naissance<i class="fas fa-table"></i></td>
                                <td><input id=date_birth name=date_birth type=date class="input-form" value="<?php form_values("date-birth") ?>"></td>
                            </tr>

                            <tr>
                               
                                <td>Mot de passe<i class="fas fa-unlock"></i></td>
                                <td><input id=pass_word name=pass_word type=pass_word placeholder="Mot de passe"
                                class="input-form"></td>
                            </tr>
                            
                            <tr>
                                <td>confirmation du MPD<i class="fas fa-lock"></i></td>
                                <td><input id=password-confirm name=password_confirm type=password
                                placeholder="Confirmer votre mot de passe" class="input-form"></td>
                            </tr>
                                                                            
                            <tr>
                                <td class="td-buttom" colspan="2">
                                    <button class="user-ajust-buttom" name="formRegister" type="submit">S'inscrire</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </section> 
        </div>        
    </main>

<?php
    include_once './includes/elements/footer.php';
?> 