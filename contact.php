<?php
    include_once './includes/functions/data_connector/db-connector.php'; 
    include_once './includes/functions/function-contact.php';
    include_once './includes/elements/header.php';   
?>

    <main>
        <h2 class="dash-h2 container">Comment pouvons-nous aider?</h2> 
        <p class="container"></p>       
        <section class="content container">
            <h2><span class="ico_user"></span>Contactez-nous</h2>
            <?php
                if (isset($error)) { echo "<div class='error-php'>" . $error . "</div>"; }
                
                if (isset($msgSuccess)) { echo "<div class='succes-php'>" . $msgSuccess . "</div>"; }
            ?>
            <form id="update-account" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                <table>
                    <tbody>
                        <tr>
                            <td>Nom<i class="fas fa-user-alt"></i></td>
                            <td><input class="input-table" name="first_name" type="text" value="<?php form_values("first_name") ?>"></td>
                        </tr>

                        <tr>
                            <td>Prenom<i class="fas fa-user-alt"></i></span></td>
                            <td><input class="input-table" name="last_name" type="text" value="<?php form_values("last_name") ?>"></td>
                        </tr>
                        
                        <tr>
                            <td>Adresse email<i class="fas fa-envelope"></i></td>
                            <td><input class="input-table" name="email" type="text" value="<?php form_values("email") ?>"></td>
                        </tr>

                        <tr>
                            <td>Commentaire<i class="fas fa-comments"></i></td>
                            <td> <textarea class="input-table" name="comment" id="comment" placeholder="Entrez votre commentaire ici!" 
                                  cols="30" rows="10"></textarea>
                            </td>
                        </tr>
                                                                       
                        <tr>
                            <td class="td-buttom" colspan="2">
                            <button class="ajust-buttom" name="formComments" type="submit">Envoyer</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </section>         
    </main>

<?php
    include_once './includes/elements/footer.php';
?> 